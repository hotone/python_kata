import statistics

L = [1.0, 2.0]

for i in range(46):
    L.append(round((L[-1] + L[-2]) / (L[-1] - L[-2]), 2))

print(sorted(L))
print(f"List length: {len(L)}")
print(f"List mean: {statistics.mean(L)}")
print(f"List median: {statistics.median(L)}")

set_form_list = set(L)

if len(L) == len(set_form_list):
    print("There is no duplicates in the list!")
else:
    for element in set_form_list:
        element_count = L.count(element)
        if element_count > 1:
            print(f"Element {element} appeared {element_count} times in the list.")
