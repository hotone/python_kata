
def find_all_numbers_divisible_by_7_and_indivisible_by_5_from_range(range_start, range_end):
    result = []
    for number in range(range_start, range_end + 1):
        if number % 7 == 0 and number % 5 != 0:
            result.append(number)
    return result


if __name__ == "__main__":
    numbers = find_all_numbers_divisible_by_7_and_indivisible_by_5_from_range(500, 3000)
    print(numbers)