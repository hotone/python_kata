import time


def measure_time(f):
    def wrap(*args, **kwargs):
        start_time = time.clock()
        function_return_value = f(*args, **kwargs)
        elapsed_time = time.clock()
        elapsed_time = elapsed_time - start_time
        print(f"Time of execution for function {f.__name__}: {elapsed_time * 1000.0} ms")
        return function_return_value
    return wrap


@measure_time
def iterated_object_for(iterated_object):
    for element in iterated_object:
        print(f"--> {element}")


@measure_time
def cpp_style_for(list_to_iterate):
    for i in range(len(list_to_iterate)):
        print(f"--> {list_to_iterate[i]}")


if __name__ == "__main__":
    testlist = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    print(f"Test list: {testlist}")

    print(f"Test for iterated object for loop:")
    iterated_object_for(testlist)

    print(f"Test for c++ style for loop:")
    cpp_style_for(testlist)

