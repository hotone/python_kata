import os
import random
import time
import inquirer


class Game:

    def __init__(self, mode):
        self.mode = mode
        self.board = [
            [" ", " ", " "],
            [" ", " ", " "],
            [" ", " ", " "],
        ]
        self.turn = "X"
        self.winner = ""
        self.__print_plane()
        self.ai = AI()

    def __print_plane(self):
        def clear_terminal():
            os.system("clear")

        clear_terminal()
        game_plane = f"""
        -------------------
        | X\\Y | 0 | 1 | 2 |
        -------------------
        |  0  | {self.board[0][0]} | {self.board[0][1]} | {self.board[0][2]} |
        -------------------
        |  1  | {self.board[1][0]} | {self.board[1][1]} | {self.board[1][2]} |
        -------------------
        |  2  | {self.board[2][0]} | {self.board[2][1]} | {self.board[2][2]} |
        -------------------
        """
        print(game_plane)

    def __check_whether_field_available(self, coordinate_x, coordinate_y):
        if self.board[coordinate_x][coordinate_y] != " ":
            return False
        else:
            return True

    def get_move_coordinate_for_player(self, player):
        proper_coordinates_passed = False
        while not proper_coordinates_passed:
            coordinates = input(f"Player {player} gives his move as X,Y: ")
            coordinates = coordinates.replace(' ', '').split(",")
            if len(coordinates) != 2:
                print(f"Exactly 2 coordinates required!")
                continue
            coordinate_x, coordinate_y = int(coordinates[0]), int(coordinates[1])
            if self.__check_whether_field_available(coordinate_x, coordinate_y):
                proper_coordinates_passed = True
            else:
                print(f"Field {coordinate_x}, {coordinate_y} not available please try choose again!")
        print(f"Player {player} made move: {coordinate_x}, {coordinate_y}")
        return coordinate_x, coordinate_y

    def get_move_coordinate_for_ai(self, difficulty_level="easy"):
        proper_coordinates_passed = False
        while not proper_coordinates_passed:
            if difficulty_level == "easy":
                coordinate_x, coordinate_y = self.ai.take_lucky_shot()
            elif difficulty_level == "hard":
                coordinate_x, coordinate_y = self.ai.find_best_move(self.board)
            if self.__check_whether_field_available(coordinate_x, coordinate_y):
                proper_coordinates_passed = True
        print(f"Computer made move: {coordinate_x}, {coordinate_y}")
        return coordinate_x, coordinate_y

    def update_players_position_for_player(self, player, cordinate_x, cordinate_y):
        if player == "X":
            self.board[cordinate_x][cordinate_y] = "X"
            self.turn = "O"
            self.__print_plane()
        elif player == "O":
            self.board[cordinate_x][cordinate_y] = "O"
            self.turn = "X"
            self.__print_plane()

    def check_whether_someone_won(self):
        players = ("X", "O")
        for player in players:
            # scan vertical lines
            for column in (0, 1, 2):
                if self.board[column][0] == player and self.board[column][1] == player and self.board[column][2] == player:
                    self.winner = player
                    return True
            # scan horizontal lines
            for row in (0, 1, 2):
                if self.board[0][row] == player and self.board[1][row] == player and self.board[2][row] == player:
                    self.winner = player
                    return True
            # scan diagonal decreasing
            if self.board[0][0] == player and self.board[1][1] == player and self.board[2][2] == player:
                self.winner = player
                return True
            # scan diagonal increasing
            if self.board[2][0] == player and self.board[1][1] == player and self.board[0][2] == player:
                self.winner = player
                return True
        return False

    def check_whether_board_is_full(self):
        if ' ' not in [item for row in self.board for item in row]:
            return True
        else:
            return False


class AI:

    def __init__(self):
        self.player = "O"
        self.opponent = "X"

    def take_lucky_shot(self):
        return random.choice((0, 1, 2)), random.choice((0, 1, 2))

    def __evaluate(self, board):
        for row in range(3):
            if board[row][0] == board[row][1] and board[row][1] == board[row][2]:
                if board[row][0] == self.player:
                    return 10
                elif board[row][0] == self.opponent:
                    return -10

        for col in range(3):

            if board[0][col] == board[1][col] and board[1][col] == board[2][col]:
                if board[0][col] == self.player:
                    return 10
                elif board[0][col] == self.opponent:
                    return -10

            if board[0][0] == board[1][1] and board[1][1] == board[2][2]:
                if board[0][0] == self.player:
                    return 10
                elif board[0][0] == self.opponent:
                    return -10

            if board[0][2] == board[1][1] and board[1][1] == board[2][0]:
                if board[0][2] == self.player:
                    return 10
                elif board[0][2] == self.opponent:
                    return -10
            return 0

    def __is_move_left(self, board):
        for i in range(3):
            for j in range(3):
                if board[i][j] == " ":
                    return True
        return False

    def __minmax(self, board, depth, is_max):
        score = self.__evaluate(board)

        if score == 10:
            return score
        if score == -10:
            return score
        if not self.__is_move_left(board):
            return 0
        if is_max:
            best = -1000

            for i in range(3):
                for j in range(3):
                    if board[i][j] == " ":
                        board[i][j] = self.player
                        best = max(best, self.__minmax(board,
                                                       depth + 1,
                                                       not is_max))
                        board[i][j] = " "
            return best
        else:
            best = 1000

            for i in range(3):
                for j in range(3):
                    if board[i][j] == " ":
                        board[i][j] = self.opponent
                        best = min(best, self.__minmax(board,
                                                       depth + 1,
                                                       not is_max))
                        board[i][j] = " "
            return best

    def find_best_move(self, board):
        best_value = -1000
        best_move = (-1, -1)

        for i in range(3):
            for j in range(3):
                if board[i][j] == " ":

                    board[i][j] = self.player

                    move_value = self.__minmax(board, 0, False)

                    board[i][j] = " "

                    if move_value > best_value:
                        best_move = (i, j)
                        best_value = move_value

        return best_move[0], best_move[1]


if __name__ == "__main__":
    def clear_terminal():
        os.system("clear")


    clear_terminal()

    game_mode_questions = [
        inquirer.List(
            "mode",
            message="What game mode would you like to play?",
            choices=["singleplayer", "multiplayer"],
        ),
    ]

    singleplayer_difficulty_level_questions = [
        inquirer.List(
            "singleplayer_difficulty",
            message="What difficulty level would you like to play?",
            choices=["easy", "hard"],
        ),
    ]

    game_mode_answers = inquirer.prompt(game_mode_questions)

    if game_mode_answers['mode'] == "singleplayer":
        difficulty_level_answers = inquirer.prompt(singleplayer_difficulty_level_questions)

    game = Game(game_mode_answers['mode'])
    if game.mode == 'multiplayer':
        while not game.check_whether_someone_won():
            if game.check_whether_board_is_full():
                print("There is a tie and nobody won!")
                break
            x, y = game.get_move_coordinate_for_player(game.turn)
            game.update_players_position_for_player(game.turn, x, y)
        if game.winner != "":
            print(f"Player {game.winner} won!")
    else:
        while not game.check_whether_someone_won():
            if game.check_whether_board_is_full():
                print("There is a tie and nobody won!")
                break
            if game.turn == "X":
                x, y = game.get_move_coordinate_for_player(game.turn)
                game.update_players_position_for_player(game.turn, x, y)
            elif game.turn == "O":
                print("Computer is making move...")
                time.sleep(2)
                x, y = game.get_move_coordinate_for_ai(difficulty_level=difficulty_level_answers['singleplayer_difficulty'])
                game.update_players_position_for_player(game.turn, x, y)
        if game.winner != "":
            print(f"Player {game.winner} won!")
